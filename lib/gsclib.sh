#!/bin/sh
# Git shell commands libraries
# Copyright (C) 2021 Márcio Silva <coadde@hyperbola.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

function gslib_checkf
{
	# Check if 'sed' is installled.
	if [ $(command -v sed > /dev/null) ]; then
		printf 'Error!: sed executable not found.\n'
		return 1
	fi

	case $USER	in
		git)	GSCMD_PATH=/srv/$USER/		;;
		team)	GSCMD_PATH=/srv/git/~$USER/	;;
		*)	GSCMD_PATH=/srv/git/~~$USER/	;;
	esac

	# Check if Git repositories path is available.
	if [ ! -e $GSCMD_PATH ]; then
		printf 'Error!: %s directory not found.\n' $GSCMD_PATH
		return 2
	elif [ ! -d $GSCMD_PATH ]; then
		printf 'Error!: %s path is not directory.\n' $GSCMD_PATH
		return 3
	elif [ ! -g $GSCMD_PATH ]; then
		printf 'Error!: %s directory requires SGID permission.\n' \
		    $GSCMD_PATH
		return 4
	elif [ -u $GSCMD_PATH ]; then
		printf 'Error!: %s directory contains SUID permission.\n' \
		    $GSCMD_PATH
		return 5
	elif [ ! -r $GSCMD_PATH ]; then
		printf 'Error!: %s directory is not readable.\n' $GSCMD_PATH
		return 7
	elif [ ! -x $GSCMD_PATH ]; then
		printf 'Error!: %s directory is not accessable.\n' $GSCMD_PATH
		return 9
	fi

	case $USER	in
		git)	if [ ! -k $GSCMD_PATH ]; then
				printf \
				    'Error!: %s directory requires sticky bit permission.\n' \
				    $GSCMD_PATH
				return 6
			elif [ -w $GSCMD_PATH ]; then
				printf 'Error!: %s directory is writable.\n' \
				    $GSCMD_PATH
				return 8
			fi
			;;
		*)	if [ -k $GSCMD_PATH ]; then
				printf \
				    'Error!: %s directory contains sticky bit permission.\n' \
				    $GSCMD_PATH
				return 6
			elif [ ! -w $GSCMD_PATH ]; then
				printf \
				    'Error!: %s directory is not writable.\n' \
				    $GSCMD_PATH
				return 8
			fi
			;;
	esac

	return 0
}
