#!/bin/sh
# Git shell command for list of Git repositories
# Copyright (C) 2021 Márcio Silva <coadde@hyperbola.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Check if 'dirname' is installled.
if [ $(command -v dirname > /dev/null) ]; then
	printf 'Error!: dirname executable not found.\n'
	exit 1
# Check if 'find' is installled.
elif [ $(command -v find > /dev/null) ]; then
	printf 'Error!: find executable not found.\n'
	exit 1
fi

source $(dirname $0)/lib/gsclib.sh

function gslib_lsgitf
{
	# Execute 'gslib_checkf' shell function and
	# import GSCMD_PATH variable.
	gslib_checkf

	# Import error status number from 'gslib_checkf'.
	local LOCAL_ERROR=$?
	case $LOCAL_ERROR	in
		0)		cd $GSCMD_PATH		;;
		*)		return $LOCAL_ERROR	;;
	esac
	unset LOCAL_ERROR

	# Print a list of Git repositories without './' and '/HEAD'.
	find . -name HEAD | sed 's|^[.][/]||; s|[/]HEAD||'

	# Import error status number from 'find' command.
	local LOCAL_EFIND=$?
	case $LOCAL_EFIND	in
		0)					;;
		*)		return 10		;;
	esac
	unset LOCAL_EFIND

	return 0
}

# Execute 'gslib_lsgitf' shell function.
gslib_lsgitf

# Exit with error status number from 'gslib_lsgitf'.
exit $?
