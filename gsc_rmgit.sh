#!/bin/sh
# Git shell command for remove a Git repository
# Copyright (C) 2021 Márcio Silva <coadde@hyperbola.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Check if 'dirname' is installled.
if [ $(command -v dirname > /dev/null) ]; then
	printf 'Error!: dirname executable not found.\n'
	exit 1
# Check if 'find' is installled.
elif [ $(command -v find > /dev/null) ]; then
	printf 'Error!: find executable not found.\n'
	exit 1
# Check if 'mkdir' is installled.
elif [ $(command -v mkdir > /dev/null) ]; then
	printf 'Error!: mkdir executable not found.\n'
	exit 1
# Check if 'rm' is installled.
elif [ $(command -v rm > /dev/null) ]; then
	printf 'Error!: rm executable not found.\n'
	exit 1
# Check if 'rmdir' is installled.
elif [ $(command -v rmdir > /dev/null) ]; then
	printf 'Error!: rmdir executable not found.\n'
	exit 1
fi

source $(dirname $0)/lib/gsclib.sh

function gslib_rmgitf
{
	# Execute 'gslib_checkf' shell function and
	# import GSCMD_PATH variable.
	gslib_checkf

	# Import error status number from 'gslib_checkf'.
	local LOCAL_ERROR=$?
	case $LOCAL_ERROR	in
		0)		cd $GSCMD_PATH		;;
		*)		return $LOCAL_ERROR	;;
	esac
	unset LOCAL_ERROR

	# Print a list of Git repositories without './' and '/HEAD'.
	local LOCAL_RMLIST
	LOCAL_RMLIST=($(find . -name HEAD | sed 's|^[.][/]||; s|[/]HEAD||'))

	# Import error status number from 'find' command.
	local LOCAL_EFIND=$?
	case $LOCAL_EFIND	in
		0)					;;
		*)		return 10		;;
	esac
	unset LOCAL_EFIND

	local LOCAL_LIST
	local LOCAL_NUMBER=0
	for LOCAL_LIST in ${LOCAL_RMLIST[@]}; do
		printf "%i %s\n" $LOCAL_NUMBER $LOCAL_LIST
		LOCAL_NUMBER=$(($LOCAL_NUMBER + 1))
	done
	unset LOCAL_LIST
	unset LOCAL_NUMBER

        printf 'Please specify the Git repository number to remove it:\n'
        local LOCAL_NUMBER
        read LOCAL_NUMBER

	# Import error status number from 'read' builtin command.
	local LOCAL_EREAD=$?
	case $LOCAL_EREAD	in
		0)					;;
		*)		return 11		;;
	esac
	unset LOCAL_EREAD

	printf 'Are you sure you want to delete this %s?\n' ${LOCAL_RMLIST[$LOCAL_NUMBER]}
        local LOCAL_CONFIRM
        read LOCAL_CONFIRM

	# Import error status number from 'read' builtin command.
	local LOCAL_EREAD=$?
	case $LOCAL_EREAD	in
		0)					;;
		*)		return 11		;;
	esac
	unset LOCAL_EREAD

	case $LOCAL_CONFIRM	in
		1)									;&
		[yY])									;&
		[yY][eE])								;&
		[yY][eE][sS])								;&
		[tT])									;&
		[tT][rR])								;&
		[tT][rR][uU])								;&
		[tT][rR][uU][eE])	rm -rfv ${LOCAL_RMLIST[$LOCAL_NUMBER]}
					# Hotfix to delete unused directories
					mkdir ${LOCAL_RMLIST[$LOCAL_NUMBER]}
					rmdir -p ${LOCAL_RMLIST[$LOCAL_NUMBER]}		;;
		*)			printf 'The remove procedure was canceled\n'	;;
	esac

	return 0
}

# Execute 'gslib_rmgitf' shell function.
gslib_rmgitf

# Exit with error status number from 'gslib_rmgitf'.
exit $?
